## Getting nodal connectivity at the vessel surface
## ***** Need to Request COORD as a field output when running the job*********##
## ***** Removed Dependency on Element set defintion 'Skin*********##




from abaqus import *
from abaqusConstants import *
import __main__
import math
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import time
import os
import sys
import ctypes
import multiprocessing
start = time.time()
path = os.getcwd()
from odbAccess import *
import glob
import os.path
import numpy as np
import copy


modelName = 'Model-1'
instanceName = 'VESSEL1_CUT_ADDLAYER-1'
#upperName= inst.upper()
a = mdb.models[modelName].rootAssembly
odb=openOdb('C:/Users/abuganza/Desktop/VivekSree/Results/BMMB_Review/Stiffness/New_Bulk/Vessel1_comp_mu5.odb')
RVE = odb.rootAssembly.instances[instanceName]
'''
## Code for writing VRML file##
session.linkedViewportCommands.setValues(_highlightLinkedViewports=True)
leaf = dgo.LeafFromElementSets(elementSets=('SKIN', ))
session.viewports['Viewport: 1'].odbDisplay.displayGroup.intersect(leaf=leaf)
session.writeVrmlFile(
fileName='C:/Users/abuganza/Desktop/Vivek Sree/ABAQUS/Postprocessing/test.wrl', 
compression=0, canvasObjects=(session.viewports['Viewport: 1'], ))
'''
#frames = [0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,92,96,100]

frames = [0,2,4,6,8,10,12,14,16,18,20]
skinel = [ v for v in RVE.elements if v.type == 'M3D3'] # Element set 

# skin_nodes = [] ## Stores Nodal connectivity of the element set SKIN
# for i in skinel.elements[0]:
# 	skin_nodes.append(i.connectivity)



for frm in frames:
	coordfield = odb.steps['Step-1'].frames[frm].fieldOutputs['COORD'].getSubset(position=NODAL)
	coordvalues = coordfield.values
	with open('C:/Users/abuganza/Desktop/VivekSree/Results/BMMB_Review/Stiffness/New_Bulk/Results2/Vessel1_comp_mu5_'+str(frm)+'.stl','w+') as the_file:
		the_file.write('solid Vessel1'+str(frm)+'\n')
	for i in skinel:
		v1 = i.connectivity[0];v2 = i.connectivity[1];v3 = i.connectivity[2]
		v1x = coordvalues[v1-1].data[0]
		v1y = coordvalues[v1-1].data[1]
		v1z = coordvalues[v1-1].data[2]
		v2x = coordvalues[v2-1].data[0]
		v2y = coordvalues[v2-1].data[1]
		v2z = coordvalues[v2-1].data[2]
		v3x = coordvalues[v3-1].data[0]
		v3y = coordvalues[v3-1].data[1]
		v3z = coordvalues[v3-1].data[2]
		n1 = (v2y-v1y)*(v3z-v1z)-(v3y-v1y)*(v2z-v1z)
		n2 = (v2z-v1z)*(v3x-v1x)-(v2x-v1x)*(v3z-v1z)
		n3 = (v2x-v1x)*(v3y-v1y)-(v3x-v1x)*(v2y-v1y)
		l = np.sqrt(n1**2+n2**2+n3**2)
		n1=n1/l;n2=n2/l;n3=n3/l
		with open('C:/Users/abuganza/Desktop/VivekSree/Results/BMMB_Review/Stiffness/New_Bulk/Results2/Vessel1_comp_mu5_'+str(frm)+'.stl','a') as the_file:
			the_file.write('facet normal '+str(n1)+' '+str(n2)+' '+str(n3)+'\n')
			the_file.write('\touter loop\n')
			the_file.write('\t\t'+'vertex '+str(v1x)+' '+str(v1y)+' '+str(v1z)+'\n')
			the_file.write('\t\t'+'vertex '+str(v3x)+' '+str(v3y)+' '+str(v3z)+'\n')
			the_file.write('\t\t'+'vertex '+str(v2x)+' '+str(v2y)+' '+str(v2z)+'\n')
			the_file.write('\tendloop\n')
			the_file.write('endfacet\n')
	with open('C:/Users/abuganza/Desktop/VivekSree/Results/BMMB_Review/Stiffness/New_Bulk/Results2/Vessel1_comp_mu5_'+str(frm)+'.stl','a') as the_file:
		the_file.write('endsolid Vessel1'+str(frm)+'\n')

odb.close()


