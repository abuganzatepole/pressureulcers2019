************************************************
Author : Vivek D Sree
************************************************

1. Sample ABAQUS input file is provided here is of normal compression and shear of the RVE 
2. ABAQUS macros are used in post processing to extract the deformed surface of the blood vessel inclusions. Run the Vessel_v2.py script within the ABAQUS python shell to extract the blood vessel surface as an STL file. 
3. The deformed volume of the solid tissue in RVE can be extracted by running the RVE_Volume script within ABAQUS python shell
