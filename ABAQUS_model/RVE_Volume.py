## Getting nodal connectivity at the vessel surface
## ***** Need to Request COORD as a field output when running the job*********##

from abaqus import *
from abaqusConstants import *
import __main__
import math
import section
import regionToolset
import displayGroupMdbToolset as dgm
import part
import material
import assembly
import step
import interaction
import load
import mesh
import job
import sketch
import visualization
import xyPlot
import displayGroupOdbToolset as dgo
import connectorBehavior
import time
import os
import sys
import ctypes
import multiprocessing
start = time.time()
path = os.getcwd()
from odbAccess import *
import glob
import os.path
import numpy as np
import copy
modelName = 'Model-1'
instanceName = 'VESSEL1_CUT_ADDLAYER-1'
#upperName= inst.upper()
a = mdb.models[modelName].rootAssembly
odb=openOdb('C:/Users/abuganza/Desktop/VivekSree/Results/BMMB_Review/Stiffness/New_Bulk/Vessel1_comp_mu5.odb')
RVE = odb.rootAssembly.instances[instanceName]


nodes = RVE.nodes

Tetels = [ v for v in RVE.elements if v.type == 'C3D4']

frm = [0,2,4,6,8,10,12,14,16,18,20]

voltot = np.zeros(len(frm))
c =0
for j in frm:
	coordfield = odb.steps['Step-1'].frames[j].fieldOutputs['COORD'].getSubset(position=NODAL)
	coordvalues = coordfield.values
	for i in Tetels:
		v1 = i.connectivity[0];v2 = i.connectivity[1];v3 = i.connectivity[2];v4 = i.connectivity[3];
		v1x = coordvalues[v1-1].data[0]
		v1y = coordvalues[v1-1].data[1]
		v1z = coordvalues[v1-1].data[2]
		v2x = coordvalues[v2-1].data[0]
		v2y = coordvalues[v2-1].data[1]
		v2z = coordvalues[v2-1].data[2]
		v3x = coordvalues[v3-1].data[0]
		v3y = coordvalues[v3-1].data[1]
		v3z = coordvalues[v3-1].data[2]
		v4x = coordvalues[v4-1].data[0]
		v4y = coordvalues[v4-1].data[1]
		v4z = coordvalues[v4-1].data[2]
		n1 = (v2y-v1y)*(v3z-v1z)-(v3y-v1y)*(v2z-v1z)
		n2 = (v2z-v1z)*(v3x-v1x)-(v2x-v1x)*(v3z-v1z)
		n3 = (v2x-v1x)*(v3y-v1y)-(v3x-v1x)*(v2y-v1y)
		voltot[c] = voltot[c] + np.abs((v4x-v1x)*n1+(v4y-v1y)*n2+(v4z-v1z)*n3)/6.
	c =c+1


for k in range(0,len(voltot)):
	with open('C:/Users/abuganza/Desktop/VivekSree/Results/BMMB_Review/Stiffness/New_Bulk/Results/RVE1_vol_mu5.txt','a') as the_file:
		the_file.write(str(voltot[k])+'\n')



odb.close()
	